package org.dhbw.mosbach.ai.inf13.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.dhbw.mosbach.ai.inf13.dao.VisitorDao;
import org.dhbw.mosbach.ai.inf13.model.Visitor;

@ApplicationScoped
@Path("/visitor")
public class VisitorRestService
{
	@Inject
	private VisitorDao visitorDao;

	@GET
	@Path("/all")
	@Produces("text/xml")
	public Visitor[] getAllVisitors()
	{
		final List<Visitor> allVisitors = this.visitorDao.getAll();

		return allVisitors.toArray(new Visitor[allVisitors.size()]);
	}

	@GET
	@Path("/{visitorName}")
	@Produces("text/xml")
	public Visitor getVisitor(@PathParam("visitorName") String visitorName)
	{
		return this.visitorDao.findByUnique("visitorName", visitorName);
	}

	@POST
	@Consumes("text/xml")
	public void addVisitor(Visitor visitor)
	{
		this.visitorDao.persist(visitor);
	}

	@DELETE
	@Path("/{visitorName}")
	public void deleteVisitor(@PathParam("visitorName") String visitorName)
	{
		final Visitor visitor = this.getVisitor(visitorName);
		this.visitorDao.remove(visitor);
	}
}
