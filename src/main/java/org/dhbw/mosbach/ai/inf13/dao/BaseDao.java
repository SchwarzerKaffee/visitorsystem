package org.dhbw.mosbach.ai.inf13.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.google.common.reflect.TypeToken;

/**
 * 
 * Provides interface between {@link E}s and database objects.
 *
 * @param <E> Type of Entity
 * @param <I> Type of serializable ID
 */
public abstract class BaseDao<E, I> implements Serializable
{
	private static final long serialVersionUID = 1L;

	@PersistenceContext
	protected EntityManager em;

	protected Class<?> entityClass;

	public BaseDao()
	{
		final TypeToken<E> type = new TypeToken<E>(this.getClass())
		{
			private static final long serialVersionUID = 1L;
		};

		this.entityClass = type.getRawType();
	}

	/**
	 * Persists {@link E} as database object
	 * @param entity {@link E} to persist
	 */
	@Transactional
	public void persist(E entity)
	{
		this.em.persist(entity);
	}

	/**
	 * Persists multiple {@link E}s as database objects.
	 * @param entities {@link E}s to persist
	 */
	@Transactional
	public void persist(@SuppressWarnings("unchecked") E... entities)
	{
		for (final E entity : entities)
		{
			persist(entity);
		}
	}

	/**
	 * Removes {@link E} from database
	 * @param entity {@link E} to remove
	 */
	@Transactional
	public void remove(E entity)
	{
		this.em.remove(em.contains(entity) ? entity : em.merge(entity));
	}

	/**
	 * Merges {@link E} with existing database object
	 * @param entity {@link E} to merge
	 * @return Merged {@link E}
	 */
	@Transactional
	public E merge(E entity)
	{
		return this.em.merge(entity);
	}

	/**
	 * Finds {@link E} by id
	 * @param id id of the {@link E} to find
	 * @return found {@link E}
	 */
	@SuppressWarnings("unchecked")
	public E findById(I id)
	{
		return (E) this.em.find(this.entityClass, id);
	}

	/**
	 * Finds {@link E} based on unique attribute
	 * @param fieldName Name of the attribute
	 * @param key Value of the attribute
	 * @return found {@link E}
	 */
	public E findByUnique(String fieldName, Object key)
	{
		final String query = String.format("FROM %s e WHERE e.%s = :key", this.entityClass.getName(), fieldName);

		@SuppressWarnings("unchecked")
		final List<E> resultList = this.em.createQuery(query).setParameter("key", key).getResultList();

		return resultList.isEmpty() ? null : resultList.get(0);
	}

	/**
	 * Gets list of all {@link E}s
	 * @return list of {@link E}s
	 */
	@SuppressWarnings("unchecked")
	public List<E> getAll()
	{
		final String query = String.format("FROM %s e", entityClass.getName());

		return this.em.createQuery(query).getResultList();
	}

	/**
	 * Getter for id of {@link E}
	 * @param e {@link E} to get id from
	 * @return id from {@link E}
	 */
	public I getId(E e)
	{
		return Tools.<I> getEntityKey(e);
	}
}
