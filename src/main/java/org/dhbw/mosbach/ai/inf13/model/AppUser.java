package org.dhbw.mosbach.ai.inf13.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

import com.google.common.collect.Sets;

@Entity
public class AppUser
{
	private long id;

	private String loginId;
	private String password;

	private String name;

	private Set<AppRole> roles = Sets.newHashSet();

	public AppUser()
	{
		super();
	}

	public AppUser(String loginId, String name)
	{
		super();
		this.loginId = loginId;
		this.name = name;
	}

	@Id
	@GeneratedValue
	@XmlTransient
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Column(nullable = false, length = 32, unique = true)
	@XmlAttribute(required = true)
	public String getLoginId()
	{
		return loginId;
	}

	public void setLoginId(String loginId)
	{
		this.loginId = loginId;
	}

	@Column(length = 256)
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	@Column(length = 64)
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	public Set<AppRole> getRoles()
	{
		return roles;
	}

	public void setRoles(Set<AppRole> roles)
	{
		this.roles = roles;
	}
}
