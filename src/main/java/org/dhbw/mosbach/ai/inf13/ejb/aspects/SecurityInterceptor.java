package org.dhbw.mosbach.ai.inf13.ejb.aspects;

import java.lang.annotation.Annotation;
import java.security.Principal;
import java.util.Optional;

import javax.annotation.Priority;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.annotation.security.RunAs;
import javax.ejb.EJBContext;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import org.dhbw.mosbach.ai.inf13.model.Roles;

@Interceptor
@CdiRoleCheck
@Priority(Interceptor.Priority.APPLICATION)
public class SecurityInterceptor
{
	private HttpServletRequest getRequest()
	{
		final FacesContext fc = FacesContext.getCurrentInstance();
		if (fc == null)
		{
			return null;
		}
		final ExternalContext ec = fc.getExternalContext();
		return (ec != null) ? (HttpServletRequest) ec.getRequest() : null;
	}

	/**
	 * @return the {@link EJBContext}.
	 */
	private EJBContext getEJBContext()
	{
		try
		{
			final InitialContext ic = new InitialContext();
			return (EJBContext) ic.lookup("java:comp/EJBContext");
		}
		catch (final NamingException e)
		{
			// this can happen if no ejb context is active (e.g., CDI bean)
			return null;
		}
	}

	@AroundInvoke
	public Object checkRoles(InvocationContext ctx) throws Exception
	{
		final RolesAllowed allowedRoles = getAllowedRoles(ctx);

		final String methodName = String.format("%s.%s", ctx.getTarget().getClass().getSimpleName(),
				ctx.getMethod().getName());

		final boolean methodHasPermitAll = methodHasAnnotation(ctx, PermitAll.class);
		final RunAs runAs = lookupAnnotation(ctx, RunAs.class);
		final boolean runsAsAdmin = runsAsAdmin(runAs);

		final HttpServletRequest request = getRequest();
		final EJBContext context = getEJBContext();
		
		final Optional<Principal> principal = Optional.ofNullable(getPrincipal(request, context));
		principal.map(p -> p.getName()).orElse("ANONYMOUS");

		boolean accessAllowed = methodHasPermitAll || runsAsAdmin;

		if ((allowedRoles != null) && !accessAllowed)
		{
			for (final String role : allowedRoles.value())
			{
				accessAllowed = isUserInRole(role, request, context);
				if (accessAllowed)
					break;
			}
		}

		if (accessAllowed)
		{
			return ctx.proceed();
		}
		else
		{
			throw new SecurityException(String.format("access to method %s not allowed", methodName));
		}
	}

	/**
	 * Looks up request or context (if request is null) to check if current user
	 * has the given role.
	 *
	 * @param role
	 *          role
	 * @param request
	 *          {@link HttpServletRequest}
	 * @param context
	 *          EJB context
	 * @return
	 */
	private boolean isUserInRole(String role, HttpServletRequest request, EJBContext context)
	{
		return (request != null) ? request.isUserInRole(role) : (context != null) ? context.isCallerInRole(role) : false;
	}

	/**
	 * Tries to get the principal object of the current user. Request context will
	 * be preferred over EJB context.
	 *
	 * @param request
	 *          {@link HttpServletRequest} (nullable)
	 * @param context
	 *          {@link EJBContext} (nullable)
	 * @return prinicpal or null if there is no authenticated user
	 */
	private Principal getPrincipal(HttpServletRequest request, EJBContext context)
	{
		try
		{
			return (request != null) ? request.getUserPrincipal() : (context != null) ? context.getCallerPrincipal() : null;
		}
		catch(final IllegalStateException e)
		{
			return null;
		}
	}

	/**
	 * Checks whether the given runas annotation has role {@link Roles#ADMIN}.
	 *
	 * @param runAs
	 *          runas annotation or null if no annotation present
	 * @return true if runas annotation has role {@link Roles#ADMIN}.
	 */
	private boolean runsAsAdmin(RunAs runAs)
	{
		final Optional<String> runAsRole = Optional.ofNullable(runAs).map(r -> r.value());

		return runAsRole.isPresent() && Roles.GATEWAYMAN.equalsIgnoreCase(runAsRole.get());
	}

	/**
	 * Returns {@link RolesAllowed} annotation of the invoked method. If the
	 * invoked method has no annotation, the target object's annotation will be
	 * used.
	 *
	 * @param ctx
	 *          context
	 * @return {@link RolesAllowed} or null if none found
	 */
	private RolesAllowed getAllowedRoles(InvocationContext ctx)
	{
		return lookupAnnotation(ctx, RolesAllowed.class);
	}

	/**
	 * Looks for the given annotation in the method call and also in the class
	 * hierarchy if necessary.
	 *
	 * @param ctx
	 *          context
	 * @param annotationClass
	 *          annotation class
	 * @return annotation or null if none found
	 */
	private <T extends Annotation> T lookupAnnotation(InvocationContext ctx, Class<T> annotationClass)
	{
		T annotation = ctx.getMethod().getAnnotation(annotationClass);
		Class<?> clazz = ctx.getTarget().getClass();

		// unfortunately, we have to search the whole inheritance hierarchy, since
		// RolesAllowed will not be inherited.
		while ((annotation == null) && (clazz != Object.class))
		{
			annotation = clazz.getAnnotation(annotationClass);
			clazz = clazz.getSuperclass();
		}

		return annotation;
	}

	/**
	 * Checks whether the method to be invoked has the given annotation. Does not
	 * check if the annotation is present at class level.
	 *
	 * @param ctx
	 *          context
	 * @param annotationClass
	 *          annotation class
	 * @return true if annotation is present
	 */
	private boolean methodHasAnnotation(InvocationContext ctx, Class<? extends Annotation> annotationClass)
	{
		return ctx.getMethod().isAnnotationPresent(annotationClass);
	}
}
