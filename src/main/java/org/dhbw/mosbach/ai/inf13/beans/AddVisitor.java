package org.dhbw.mosbach.ai.inf13.beans;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.dhbw.mosbach.ai.inf13.dao.VisitorDao;
import org.dhbw.mosbach.ai.inf13.model.Visitor;


/**
 * Responsible for adding new Visitors.
 *
 */
@Named
@SessionScoped
public class AddVisitor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private VisitorDao visitorDao;

	private Visitor visitor;
	
   /**
   * Adds a new {@link Visitor} including the time of arrival.
   * @return "addvisitor".	
   */
	public String add()
	{
		this.visitor = new Visitor();
		
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		this.visitor.setArriveTime(dateFormat.format(new Date()));

		return "addvisitor";
	}

   /**
   * Saves a {@link Visitor} in the DB.
   * @return "index" Name of the XHTML	
   */
	public String save()
	{
		if(this.visitor != null)
		{
			this.visitor.setDate(new Date());
			
			this.visitorDao.persist(this.visitor);
			this.visitor = null;
		}

		return "index";
	}

   /**
   * Getter for {@link Visitor}.
   * @return {@link Visitor} selected for edit	
   */
	public Visitor getVisitor()
	{
		return this.visitor;
	}

   /**
   * Setter for {@link Visitor}.
   * @param visitor {@link Visitor} selected for edit
   */
	public void setVisitor(Visitor visitor)
	{
		this.visitor = visitor;
	}
}