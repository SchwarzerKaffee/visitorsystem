package org.dhbw.mosbach.ai.inf13.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@XmlRootElement
public class Visitor implements Serializable, Cloneable
{
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String visitorName;
	private String receiverName;
	private Date date;
	private String arriveTime;
	private String leaveTime;
	
	public Visitor()
	{
		super();
	}

	@Override
	public Visitor clone()
	{
		try
		{
			return (Visitor)super.clone();
		}
		catch (final CloneNotSupportedException e)
		{
			throw new RuntimeException("cloning is not supported by this class");
		}
	}

	@Id
	@GeneratedValue
	@XmlTransient
	public long getId()
	{
		return this.id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Column(nullable = false, length = 256)
	@XmlAttribute(required = true)
	public String getVisitorName()
	{
		return this.visitorName;
	}

	public void setVisitorName(String visitorName)
	{
		this.visitorName = visitorName;
	}
	
	@Column(nullable = false, length = 256)
	@XmlAttribute(required = true)
	public String getReceiverName()
	{
		return this.receiverName;
	}

	public void setReceiverName(String receiverName)
	{
		this.receiverName = receiverName;
	}

	public Date getDate()
	{
		return this.date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public String getArriveTime()
	{
		return this.arriveTime;
	}

	public void setArriveTime(String arriveTime)
	{
		this.arriveTime = arriveTime;
	}

	public String getLeaveTime()
	{
		return this.leaveTime;
	}

	public void setLeaveTime(String leaveTime)
	{
		this.leaveTime = leaveTime;
	}
}
