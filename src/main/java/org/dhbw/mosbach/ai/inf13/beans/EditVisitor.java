package org.dhbw.mosbach.ai.inf13.beans;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.dhbw.mosbach.ai.inf13.dao.VisitorDao;
import org.dhbw.mosbach.ai.inf13.model.Visitor;


/**
 * This class is responsible for editing visitors.
 *
 */
@Named
@SessionScoped
public class EditVisitor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private VisitorDao visitorDao;

	private Visitor visitor;
	
   /**
   * Sets the {@link Visitor} for editing.
   * @param visitor The {@link Visitor} that is selected for editing
   * @return "editvisitor" - Name of the xHTML
   */
	public String edit(Visitor visitor)
	{
		this.visitor = this.visitorDao.findById(this.visitorDao.getId(visitor));

		return "editvisitor";
	}
	
   /**
   * Deletes {@link Visitor}.
   * @param visitor {@link Visitor} for deletion.
   * @return "index" - Name of the xHTML	
   */
	public String delete(Visitor visitor)
	{
		this.visitor = this.visitorDao.findById(this.visitorDao.getId(visitor));
		this.visitorDao.remove(this.visitor);

		return "index";
	}
	
   /**
   * Checks out {@link Visitor}.
   * @param visitor {@link Visitor} for check out.
   * @return "index" - Name of the xHTML	
   */
	public String checkOut(Visitor visitor)
	{
		this.visitor = this.visitorDao.findById(this.visitorDao.getId(visitor));
		
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		this.visitor.setLeaveTime(dateFormat.format(new Date()));
		
		return this.save();
	}

   /**
   * Saves {@link Visitor}.
   * @return "index" - Name of the xHTML	
   */	
	public String save()
	{
		this.visitorDao.merge(this.visitor);

		return "index";
	}

   /**
   * Getter for {@link Visitor}.
   * @return {@link Visitor} selected for edit	
   */
	public Visitor getVisitor()
	{
		return this.visitor;
	}

   /**
   * Setter for {@link Visitor}.
   * @param visitor {@link Visitor} selected for edit
   */
	public void setVisitor(Visitor visitor)
	{
		this.visitor = visitor;
	}
}