package org.dhbw.mosbach.ai.inf13.ejb.beans;

import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.RunAs;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.dhbw.mosbach.ai.inf13.model.AppRole;
import org.dhbw.mosbach.ai.inf13.model.AppUser;
import org.dhbw.mosbach.ai.inf13.model.Roles;

@Startup
@Singleton
@RunAs(Roles.GATEWAYMAN)
public class DemoDataProvider
{
	@PersistenceContext
	private EntityManager em;

	@Resource
	private EJBContext context;

	@Resource
	private TimerService timerService;
	
	@EJB
	private AppUserDaoProxy appUserDaoProxy;

	@PostConstruct
	private void init()
	{
		// unfortunately, security lookups are not allowed during postconstruct of a
		// singleton,
		// so we have to delay this stuff...
		timerService.createSingleActionTimer(1000, new TimerConfig("ddp", false));
	}

	@Timeout
	@Transactional(TxType.REQUIRED)
	private void timer()
	{
		// Check whether any data exists
		final Long userCount = (Long) em.createQuery("SELECT COUNT(au) FROM AppUser au").getSingleResult();

		if (userCount.longValue() == 0)
		{
			createUsers();
		}
	}

	/**
	 * Creates Demo users.
	 */
	@Transactional
	private void createUsers()
	{
		// There is no appRoleDao, since these roles shall not be changed after
		// initialization
		final AppRole gatewayManRole = new AppRole(Roles.GATEWAYMAN, "Gatewayman");

		em.persist(gatewayManRole);

		createUser("admin", "Holy admin", "admin", gatewayManRole);
	}

	/**
	 * Creates a new user with given login, user name, password, and role.
	 *
	 * @param login
	 *          login
	 * @param userName
	 *          user name
	 * @param password
	 *          password
	 * @param userRoles
	 *          roles
	 * @return created user
	 */
	private AppUser createUser(String login, String userName, String password, AppRole... userRoles)
	{
		final AppUser user = new AppUser(login, userName);
		user.getRoles().addAll(Arrays.asList(userRoles));
		appUserDaoProxy.changePassword(user, password);
		appUserDaoProxy.persist(user);

		return user;
	}
}
