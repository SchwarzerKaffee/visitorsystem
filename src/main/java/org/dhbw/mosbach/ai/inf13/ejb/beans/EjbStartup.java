package org.dhbw.mosbach.ai.inf13.ejb.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

@Startup
@Singleton
public class EjbStartup
{
	@Resource private TimerService timerService;

	@PostConstruct
	private void init()
	{
		final TimerConfig timerConfig = new TimerConfig("App Startup Timer", false);
		timerService.createSingleActionTimer(120_000, timerConfig);
	}

	@Timeout
	private void startApp(Timer timer) throws Exception
	{
		timer.getInfo();
	}

	@PreDestroy
	private void stopApp()
	{
	}
}
