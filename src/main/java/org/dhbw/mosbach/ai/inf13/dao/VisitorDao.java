package org.dhbw.mosbach.ai.inf13.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.dhbw.mosbach.ai.inf13.model.Visitor;

/**
 * 
 * Provides filter capabilities and manages access to the visitor database objects.
 *
 */
@Named
@SessionScoped
public class VisitorDao extends BaseDao<Visitor, Long>
{
	private static final long serialVersionUID = 1L;
	
	private String filterVisitorName = "";
	private String filterReceiverName = "";
	private Date filterDate;

   /**
   * Gets filtered list of {@link Visitor}s
   * @return List<Visitor> Filtered list of {@link Visitor}s.	
   */
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Visitor> getFiltered()
	{	
		Date nextFilterDate = (Date)this.filterDate.clone();
		nextFilterDate.setSeconds(60);
		nextFilterDate.setMinutes(60);
		nextFilterDate.setHours(24);
		
		final String query = String.format("FROM %s v WHERE v.%s LIKE :filterVisitorName AND v.%s LIKE :filterReceiverName AND date >= :filterDate AND date <= :nextFilterDate ORDER BY date DESC", entityClass.getName(), "visitorName", "receiverName");

		final List<Visitor> resultList = this.em.createQuery(query)
				.setParameter("filterVisitorName", "%" + this.filterVisitorName + "%")
				.setParameter("filterReceiverName", "%" + this.filterReceiverName+ "%")
				.setParameter("filterDate", this.filterDate)
				.setParameter("nextFilterDate", nextFilterDate)
				.getResultList();

		return resultList.isEmpty() ? null : resultList;
	}
	
	/**
	 * Creates clone of {@link Visitor} and persists the {@link Visitor}
	 * @param visitor {@link Visitor} to persist
	 */
	@Override
	@Transactional
	public void persist(Visitor visitor)
	{
		final Visitor modifiedVisitor = visitor.clone();
		
		super.persist(modifiedVisitor);
	}
	
	/**
	 * Getter for visitor name filter
	 * @return current filter of visitor name
	 */
	public String getFilterVisitorName()
	{
		return this.filterVisitorName;
	}

	/**
	 * Setter for visitor name filter
	 * @param filterVisitorName filter to be set
	 */
	public void setFilterVisitorName(String filterVisitorName)
	{
		this.filterVisitorName = filterVisitorName;
	}
	
	/**
	 * Getter for receiver name filter
	 * @return current filter of receiver name
	 */
	public String getFilterReceiverName()
	{
		return this.filterReceiverName;
	}

	/**
	 * Setter for receiver name filter
	 * @param filterReceiverName filter to be set
	 */
	public void setFilterReceiverName(String filterReceiverName)
	{
		this.filterReceiverName = filterReceiverName;
	}
	
	/**
	 * Getter for date filter
	 * @return current filter of date
	 */
	@SuppressWarnings("deprecation")
	public String getFilterDate()
	{
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		
		if(filterDate == null)
		{
			this.filterDate = new Date();
			this.filterDate.setSeconds(0);
			this.filterDate.setMinutes(0);
			this.filterDate.setHours(0);
		}
		
		return dateFormat.format(this.filterDate);
	}

	/**
	 * Setter for date filter
	 * @param filterDate filter to be set
	 */
	public void setFilterDate(String filterDate)
	{
		if(filterDate.length() != 0)
		{
			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
			
			try
			{
				this.filterDate = dateFormat.parse(filterDate);
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
		}
	}
}