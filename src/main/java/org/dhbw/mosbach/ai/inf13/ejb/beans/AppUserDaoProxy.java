package org.dhbw.mosbach.ai.inf13.ejb.beans;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;

import org.dhbw.mosbach.ai.inf13.dao.AppUserDao;
import org.dhbw.mosbach.ai.inf13.model.AppUser;

/**
 * This class only exists to omit security checks for generating demo data.
 *
 * @author Alexander.Auch
 *
 */
@Stateless(name = "appUserDaoProxy")
@SpecialAppUserDao
@PermitAll
public class AppUserDaoProxy extends AppUserDao
{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@PermitAll
	@Override
	public void changePassword(AppUser user, String password)
	{
		super.changePassword(user, password);
	}

	@PermitAll
	@Override
	public void persist(AppUser entity)
	{
		super.persist(entity);
	}

	@PermitAll
	@Override
	public void persist(AppUser... entities)
	{
		super.persist(entities);
	}

	@PermitAll
	@Override
	public AppUser findByUnique(String fieldName, Object key)
	{
		return super.findByUnique(fieldName, key);
	}
}
