package org.dhbw.mosbach.ai.inf13.dao;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.Dependent;
import javax.inject.Named;

import org.dhbw.mosbach.ai.inf13.ejb.aspects.CdiRoleCheck;
import org.dhbw.mosbach.ai.inf13.model.AppUser;
import org.dhbw.mosbach.ai.inf13.model.Roles;
import org.jboss.security.Base64Encoder;

@Named
@Dependent
@CdiRoleCheck
@RolesAllowed(value = { Roles.GATEWAYMAN })
public class AppUserDao extends BaseDao<AppUser, Long>
{
	private static final long serialVersionUID = 1L;

	private MessageDigest getMessageDigest()
	{
		try
		{
			return MessageDigest.getInstance("SHA-256");
		}
		catch (final NoSuchAlgorithmException e)
		{
			throw new RuntimeException(e);
		}
	}

	@RolesAllowed(value = { Roles.GATEWAYMAN })
	public void changePassword(AppUser user, String password)
	{
		try
		{
			user.setPassword(Base64Encoder.encode(getMessageDigest().digest(password.getBytes())));
		}
		catch (final IOException e)
		{
		}
	}

	@Override
	/**
	 * Explicitly allow this method call for anybody.
	 */
	@PermitAll
	public AppUser findByUnique(String fieldName, Object key)
	{
		return super.findByUnique(fieldName, key);
	}
}
