package org.dhbw.mosbach.ai.inf13.beans;

import java.security.Principal;
import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dhbw.mosbach.ai.inf13.dao.AppUserDao;
import org.dhbw.mosbach.ai.inf13.model.AppUser;

@Named
@RequestScoped
public class SecurityBean
{
	@Inject
	private AppUserDao appUserDao;

	@SuppressWarnings("unused")
	private HttpServletResponse getResponse()
	{
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}

	private HttpServletRequest getRequest()
	{
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	private Optional<Principal> getPrincipal()
	{
		return Optional.ofNullable(getRequest().getUserPrincipal());
	}

	public String getLoginName()
	{
		return getPrincipal().map(p -> p.getName()).orElse("");
	}

	public boolean isAuthenticated()
	{
		return this.getPrincipal().isPresent();
	}

	public boolean isUserInRole(String role)
	{
		return this.getRequest().isUserInRole(role);
	}

	public AppUser getUser()
	{
		final Optional<Principal> principal = this.getPrincipal();

		if (principal.isPresent())
		{
			return appUserDao.findByUnique("loginId", principal.get().getName());
		}

		return null;
	}

	public String login()
	{
		return "/loginsuccess.xhtml?faces-redirect=true";
	}

	public String logout()
	{
		final HttpServletRequest request = this.getRequest();

		try
		{
			request.logout();
			request.getSession().invalidate();
		}
		catch (final ServletException e)
		{
		}

		return "index";
	}
}
